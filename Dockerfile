FROM python:3.8

WORKDIR /usr/src/app

COPY . . 
RUN chmod a+x wait-for-it.sh
RUN pip install --no-cache-dir -r requirements.txt && pip install .

CMD [ "python", "hnscraper/scraper.py" ]
