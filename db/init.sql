CREATE TABLE posts (
  id SERIAL PRIMARY KEY,
  title TEXT NOT NULL,
  url TEXT NOT NULL,
  created TIMESTAMPTZ DEFAULT now(),
  UNIQUE (title, url)
);
CREATE INDEX links_title ON posts (title);
CREATE INDEX links_url ON posts (url);
CREATE INDEX links_created ON posts (created);
