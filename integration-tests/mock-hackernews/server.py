import http.server, ssl
import json


class CustomHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        with open('data.html') as f:
            content = bytes(f.read(), 'utf-8')
        self.wfile.write(content)

server_address = ('0.0.0.0', 443)
httpd = http.server.HTTPServer(server_address, CustomHandler)
httpd.socket = ssl.wrap_socket(httpd.socket,
                               server_side=False,
                               certfile='./server.pem',
                               ssl_version=ssl.PROTOCOL_TLS)
httpd.serve_forever()
