import pytest
import requests
import json


URL = 'http://127.0.0.1:8000/posts'



@pytest.mark.parametrize('params,expected_result', [
    (
        {
            'limit': 3,
            'offset': 2,
        },
        [
            {
                "id": 3,
                "url": "https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type",
                "title": "CRDT: Conflict-free replicated data type",
            },
            {
                "id": 4,
                "url": "https://www.propublica.org/article/the-ransomware-superhero-of-normal-illinois",
                "title": "The Ransomware Superhero of Normal, Illinois",
            },
            {
                "id": 5,
                "url": "https://www.technologyreview.com/s/614642/dna-database-gedmatch-golden-state-killer-security-risk-hack/",
                "title": "DNA database that found Golden State Killer is potential national security leak",
            }
        ]
    ), (
        {
            'limit': 2,
            'offset': 5,
            'order': 'title',
        },
        [
            {
                "id": 3,
                "url": "https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type",
                "title": "CRDT: Conflict-free replicated data type",
            },
            {
                "id": 23,
                "url": "https://uxdesign.cc/design-better-data-tables-4ecc99d23356",
                "title": "Designing better data tables (2017)",
            }
        ]
    ), (
        {
            'limit': 1,
            'offset': 5,
            'order': 'url',
            'direction': 'desc',
        },
        [
            {
                "id": 4,
                "url": "https://www.propublica.org/article/the-ransomware-superhero-of-normal-illinois",
                "title": "The Ransomware Superhero of Normal, Illinois",
            }
        ]
#    ), (
    ),
])
def test_service(params, expected_result):
    r = requests.get(URL, params=params)
    assert r.status_code == 200
    actual_result = json.loads(r.text)
    for r in actual_result:
        del r['created']
    assert actual_result == expected_result

