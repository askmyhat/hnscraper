A service to fetch hacker news posts and get stored data through HTTP API.

# Dependencies

* aiopg
* aiohttp
* lxml

## Deploy with Docker

#### Requirements:

* docker>=19.03
* docker-compose>=1.24.1

#### Running:

Set environment variables:

```
$ cat .env
POSTGRES_PASSWORD=docker
```

Run a service

```
docker-compose up
```

## Deploy with virtual environment

#### Requirements:

* PostgreSQL 12+
* python 3.8+

#### Installation:

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

#### Database initialization:

```bash
psql -h <host> -U <user> -d <database> -f db/init.sql
```


#### Usage

```bash
$ python server.py -h
usage: server.py [-h] [--host HOST] [--port PORT] [--dbhost DBHOST]
                 [--dbport DBPORT] [--dbuser DBUSER] [--dbpassword DBPASSWORD]
                 [--quiet]

A web server to fetch scrapped posts

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Server IP address
  --port PORT, -p PORT  Server TCP port
  --dbhost DBHOST       Database IP address
  --dbport DBPORT       Database TCP port
  --dbuser DBUSER       Database username
  --dbpassword DBPASSWORD
                        Database password
  --quiet, -q           Don't output messages
```

```bash
$ python scraper.py -h       
usage: scraper.py [-h] [--interval INTERVAL] [--dbhost DBHOST]
                  [--dbport DBPORT] [--dbuser DBUSER]
                  [--dbpassword DBPASSWORD] [--quiet]

Hacker news scraper

optional arguments:
  -h, --help            show this help message and exit
  --interval INTERVAL, -i INTERVAL
                        Scraping time interval in seconds
  --dbhost DBHOST       Database IP address
  --dbport DBPORT       Database TCP port
  --dbuser DBUSER       Database username
  --dbpassword DBPASSWORD
                        Database password
  --quiet, -q           Don't output messages
```

## API

Swagger: /docs

### Get posts

**URL** : `/posts`

**Method** : `GET`

**Parameters**:

* **limit** (optional)
* **offset** (optional)
* **order** (optional): 'id', 'title', 'url' or 'created'
* **direction** (optional): 'asc' or 'desc'

**Response**:

* **id**: JSON with posts

**Example:**

```bash
$ curl -X GET http://localhost:8000/posts?offset=0&limit=2&order=title&direction=asc
[
  {"id": 1, "title": "Announcing Rust 1.33.0", "url": "https://example.com", "created": "ISO 8601"},
  {"id": 2, "title": "Redesigning GitHub Repository Page", "url": "https://example.com", "created": "ISO 8601"}
]
```

## Testing

```bash
python -m venv venv
source venv/bin/activate

pip install -e .
pip install -r requirements.txt

./test.sh
```
