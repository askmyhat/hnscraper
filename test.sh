#!/bin/sh

set -e

# Unit tests

# pytest -vvv tests

# Integration tests

docker-compose -f docker-compose-test.yml up -d
until $(curl --output /dev/null --silent --head --fail http://127.0.0.1:8000/posts); do
    printf '.'
    sleep 1
done
pytest integration-tests -vvv
docker-compose down -v
