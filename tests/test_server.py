import pytest
import asyncio
import json
import time
import mock
from hnscraper.server import init_app


async def async_magic():
    pass

mock.MagicMock.__await__ = lambda x: async_magic().__await__()


test_data = [
    (
        'test_scraper/hn-2019-11-06-19-15.html',
        'test_scraper/hn-2019-11-06-19-15.posts',
    ),
    (
        'test_scraper/hn-new-2019-11-06-19-15.html',
        'test_scraper/hn-new-2019-11-06-19-15.posts',
    ),
]

test_source_filenames = [i[0] for i in test_data]
test_posts_filenames = [i[1] for i in test_data]


class DB_mock(object):
    def __init__(self):
        pass

    async def open(self, *args, **kwargs):
        pass

    async def close(self, *args, **kwargs):
        pass

    async def get_posts(self, *args, **kwargs):
        yield None


@pytest.fixture
def client(loop, aiohttp_client):
    db = DB_mock()
    app = init_app(db)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.mark.parametrize('params', [
    ({
        'limit': 1000,
    }),
    ({
        'limit': -5,
    }),
    ({
        'limit': 'abc',
    }),
    ({
        'offset': -5,
    }),
    ({
        'offset': 'abc',
    }),
    ({
        'order': 123,
    }),
    ({
        'order': 'not-existing-field',
    }),
    ({
        'direction': 123,
    }),
    ({
        'direction': 'not-existing-direction',
    })
])
async def test_bad_queries(client, params):
    response = await client.get('/posts', params=params)
    assert response.status == 422
