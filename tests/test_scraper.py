import os
import pytest
import asyncio
import json
import time
import mock
from hnscraper.scraper import URL, get_page, parse_posts, start_scraper


async def async_magic():
    pass


mock.MagicMock.__await__ = lambda x: async_magic().__await__()

dirname = os.path.dirname(__file__)


test_data = [
    (
        os.path.join(dirname, 'test_scraper/hn-2019-11-06-19-15.html'),
        os.path.join(dirname, 'test_scraper/hn-2019-11-06-19-15.posts'),
    ),
    (
        os.path.join(dirname, 'test_scraper/hn-new-2019-11-06-19-15.html'),
        os.path.join(dirname, 'test_scraper/hn-new-2019-11-06-19-15.posts'),
    ),
]

test_source_filenames = [i[0] for i in test_data]
test_posts_filenames = [i[1] for i in test_data]


@pytest.mark.asyncio
async def test_get_page():
    header_links = ['Hacker News',  'new', 'past', 'comments', 'ask', 'show', 'jobs', 'submit']
    content = await get_page(URL)

    for link in header_links:
        assert link in content

    for i in range(1, 31):
        assert str(i) in content


@pytest.mark.parametrize('source_filename,posts_filename', test_data)
def test_parse_posts(source_filename, posts_filename):
    with open(source_filename) as source_file:
        with open(posts_filename) as posts_file:
            expected_posts = posts_file.read()
            actual_posts = json.dumps(list(parse_posts(source_file.read())), indent=2)
            assert expected_posts == actual_posts


async def get_page_mock(*args, **kwargs):
    if not hasattr(get_page_mock, 'test_page_number'):
        get_page_mock.test_page_number = 0
    with open(test_source_filenames[get_page_mock.test_page_number]) as f:
        get_page_mock.test_page_number += 1
        return f.read()


class DB_mock(object):
    def __init__(self):
        self.records = []
    async def write_post(self, url, title):
        current_time = int(time.time())
        record = f'{url} {title} {current_time}'
        if record not in self.records:
            self.records.append(record)


@pytest.mark.asyncio
async def test_start_scraper():
    def check_records(db, posts_filename, start_time, end_time):
        with open(posts_filename) as posts_file:
            posts = json.load(posts_file)
            for post in posts:
                url, title = post
                found_records = [record for record in db.records if f'{url} {title}' in record]
                assert len(found_records) == 1
                record_time = int(found_records[0].split()[-1])
                assert record_time >= start_time
                assert record_time < end_time
            return len(posts)

    db = DB_mock()
    current_time = int(time.time())
    interval = 2

    with mock.patch('hnscraper.scraper.get_page') as mock_get_page:
        mock_get_page.side_effect = get_page_mock
        await start_scraper(db, interval=interval, repeat=len(test_data))

    recorded_posts_len = 0
    for i, test_posts_filename in enumerate(test_posts_filenames):
        result = check_records(db, test_posts_filename, current_time + i * interval, current_time + (i + 1) * interval)
        recorded_posts_len += result

    assert recorded_posts_len == len(db.records)
