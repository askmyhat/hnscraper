import json
from datetime import datetime


from aiohttp import web
from aiohttp_apispec import request_schema
from marshmallow import Schema, fields, validate


from hnscraper.db import DB


class PostEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


class PostsRequestSchema(Schema):
    limit = fields.Integer(validate=validate.Range(min=1, max=100))
    offset = fields.Integer(validate=validate.Range(min=0))
    order = fields.String(
        validate=validate.OneOf(DB.FIELDS)
    )
    direction = fields.String(
        validate=validate.OneOf(['asc', 'desc'])
    )


@request_schema(PostsRequestSchema)
async def posts(request):
    params = {
        'limit': request.rel_url.query.get('limit'),
        'offset': request.rel_url.query.get('offset'),
        'order': request.rel_url.query.get('order'),
        'direction': request.rel_url.query.get('direction'),
    }

    result = []
    async for post in request.app['db'].get_posts(**params):
        result.append(post)
    result = json.dumps(result, indent=2, cls=PostEncoder)
    return web.Response(text=result)
