import argparse
import logging
import asyncio


from aiohttp import web
from aiohttp_apispec import (
    setup_aiohttp_apispec, 
    validation_middleware,
)


from hnscraper.utils import setup_logging
from hnscraper.db import DB
from hnscraper.views import posts


def start_server(host, port, db):
    app = init_app(db)
    web.run_app(app, host=host, port=port)


def init_app(db):
    app = web.Application()

    app['db'] = db
    app.on_startup.append(db.open)
    app.on_cleanup.append(db.close)

    setup_aiohttp_apispec(app, swagger_path='/docs')
    app.middlewares.append(validation_middleware)

    setup_routes(app)

    return app


def setup_routes(app):
    app.router.add_get('/posts', posts)


def main(args):
    setup_logging(args)
    db = DB(args.dbhost, args.dbport, args.dbuser, args.dbpassword)
    start_server(args.host, args.port, db)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A web server to fetch scrapped posts')
    parser.add_argument('--host',
                        dest='host',
                        default='127.0.0.1',
                        help='Server IP address')
    parser.add_argument('--port', '-p',
                        dest='port',
                        default='8000',
                        type=int,
                        help='Server TCP port')
    parser.add_argument('--dbhost',
                        dest='dbhost',
                        default='127.0.0.1',
                        help='Database IP address')
    parser.add_argument('--dbport',
                        dest='dbport',
                        default=5432,
                        type=int,
                        help='Database TCP port')
    parser.add_argument('--dbuser', 
                        dest='dbuser',
                        default='postgres',
                        help='Database username')
    parser.add_argument('--dbpassword',
                        dest='dbpassword',
                        default='docker',
                        help='Database password')
    parser.add_argument('--quiet', '-q',
                        dest='quiet',
                        action='store_true',
                        help='Don\'t output messages')
    args = parser.parse_args()
    main(args)
