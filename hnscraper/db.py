from collections import namedtuple
import aiopg
import psycopg2


class DB(object):
    FIELDS = ('id', 'url', 'title', 'created')

    def __init__(self, host, port, user, password):
        self.config = {
            'host': host,
            'port': port,
            'user': user,
            'password': password,
        }

    async def __aenter__(self):
        await self.open()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.close()

    async def open(self, *args, **kwargs):
        self.pool = await aiopg.create_pool(**self.config)

    async def close(self, *args, **kwargs):
        self.pool.close()
        await self.pool.wait_closed()

    async def write_post(self, url, title):
        query = 'INSERT INTO posts (url, title) VALUES (%s, %s)'
        params = (url, title)
        try:
            with await self.pool.cursor() as cur:
                await cur.execute(query, params)
        except psycopg2.errors.UniqueViolation as e:
            return False
        return True

    async def get_posts(self, limit=None, offset=None, order=None, direction=None):
        query = f'SELECT id,url,title,created FROM posts'

        if order:
            direction = direction or 'ASC'
            query += f' ORDER BY {order} {direction}'

        limit = limit or 30
        query += f' LIMIT {limit}'

        if offset:
            query += f' OFFSET {offset}'

        query += ';'

        with await self.pool.cursor() as cur:
            await cur.execute(query)
            async for post in cur:
                yield dict(zip(self.FIELDS, post))
