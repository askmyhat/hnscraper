import argparse
import logging
import time
import asyncio
import aiohttp
from urllib.parse import urlparse
from lxml import etree


from hnscraper.utils import setup_logging
from hnscraper.db import DB


logger = logging.getLogger('scraper')


URL = 'https://news.ycombinator.com/'


async def start_scraper(db, interval=600, repeat=None, skip_ssl=False):
    if repeat:
        for i in range(repeat):
            await scrape(db, skip_ssl)
            time.sleep(interval)
            logger.debug(f'Sleeping for {interval} seconds')
    else:
        while True:
            await scrape(db, skip_ssl)
            time.sleep(interval)
            logger.debug(f'Sleeping for {interval} seconds')


async def scrape(db, skip_ssl=False):
    page_source = await get_page(URL, skip_ssl)
    for url, title in parse_posts(page_source):
        logger.debug(f'Writing to db {url}')
        result = await db.write_post(url, title)
        if not result:
            logger.debug('Record exists')


async def get_page(url, skip_ssl=False):
    logger.debug('Downloading page source')
    ssl = not skip_ssl
    async with aiohttp.ClientSession() as session:
        async with session.get(url, ssl=ssl) as response:
            return await response.text()


def parse_posts(page_source):
    logger.debug('Parsing posts')
    root = etree.HTML(page_source)
    cells = root.xpath('body/center/table/tr[3]/td/table')[0]

    for cell in cells[:90:3]:
        post = cell.xpath('td[3]/a')[0]
        url = post.get('href')
        if not urlparse(url).netloc:
            url = f'{URL}{url}'
        title = post.xpath('string()')
        yield (url, title)


async def main(args):
    setup_logging(args)
    async with DB(host=args.dbhost, port=args.dbport, user=args.dbuser, password=args.dbpassword) as db:
        await start_scraper(db, args.interval, args.skip_ssl)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Hacker news scraper')
    parser.add_argument('--interval', '-i',
                        dest='interval',
                        type=int,
                        default=600,
                        help='Scraping time interval in seconds')
    parser.add_argument('--dbhost',
                        dest='dbhost',
                        default='127.0.0.1',
                        help='Database IP address')
    parser.add_argument('--dbport',
                        dest='dbport',
                        default=5432,
                        type=int,
                        help='Database TCP port')
    parser.add_argument('--dbuser',
                        dest='dbuser',
                        default='postgres',
                        help='Database username')
    parser.add_argument('--dbpassword',
                        dest='dbpassword',
                        default='docker',
                        help='Database password')
    parser.add_argument('--skip-ssl',
                        dest='skip_ssl',
                        action='store_false',
                        help='Relax certification checks')
    parser.add_argument('--quiet', '-q',
                        dest='quiet',
                        action='store_true',
                        help='Don\'t output messages')
    args = parser.parse_args()
    asyncio.run(main(args))
