import logging


def setup_logging(args):
    if not args.quiet:
        logging.basicConfig(level=logging.DEBUG)
